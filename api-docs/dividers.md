### Use this Module for dividers

```
import { TmoDividerModule } from '@tmo/shared/tmo-divider';
```

### Change the dividerType based on the type you need full-bleed, inset, or total.

```
<tmo-divider [dividerType]="'inset'"></tmo-divider> 
```

      