### Original Pricing

```
<tmo-price-lockup
  [leftAmount]="'$499.50'"
  [leftText]="'Today'"
  [leftMagenta]="'true'"
  [rightAmount]="'$80.09'"
  [rightText]="'Monthly'"
  [rightSubText]="'(x 24 mos.)'"
  [showTotalPricing]="'true'"
  [totalOriginalPrice]="'899'"
  [totalPrice]="'849'"
  [totalPriceOnly]="'false'"
  [totalPriceText]="'Total Price'"
  [sizeClass]="'price-lockup-xl'">
</tmo-price-lockup>
```          

### Total Price Only

```
<tmo-price-lockup
  [leftAmount]="'$199.50'"
  [leftText]="'Today'"
  [leftMagenta]="'true'"
  [rightAmount]="'$80.09'"
  [rightText]="'Monthly'"
  [rightSubText]="'(x 24 mos.)'"
  [showTotalPricing]="'true'"
  [totalPrice]="'889'"
  [totalPriceOnly]="'true'"
  [totalPriceText]="'Total Price'"
  [sizeClass]="'price-lockup-xl'">
</tmo-price-lockup>
```

### Payment Only

```
<tmo-price-lockup
  [leftAmount]="'$299'"
  [leftText]="'Today'"
  [rightAmount]="'$57.09'"
  [rightText]="'Monthly'"
  [leftMagenta]="'true'"
  [leftOnly]="'false'"
  [showTotalPricing]="'false'"
  [showAsDiscount]="'false'"
  [totalPriceOnly]="'false'">
</tmo-price-lockup>
```        

### Zero Pricing

```
<tmo-price-lockup
  [leftAmount]=""
  [leftText]="'Today'"
  [rightAmount]=""
  [rightText]="'Monthly'"
  [showTotalPricing]="'false'"
  [sizeClass]="'price-lockup-large'">
</tmo-price-lockup>
```              

### Stand Alone Pricing

```
<tmo-price-lockup
  [leftAmount]="'344.99'"
  [leftText]="'Today'"
  [leftOnly]="'true'"
  [leftMagenta]="'false'"
  [rightAmount]="29.88"
  [sizeClass]="'price-lockup-large'">
</tmo-price-lockup>
```          

### Discount Pricing

```
<tmo-price-lockup
  [showAsDiscount]="'true'"
  [leftAmount]="'32.99'"
  [leftMagenta]="'false'"
  [leftText]="'Original Price'"
  [rightAmount]="29.88"
  [rightText]="'Today'"
  [sizeClass]="'price-lockup-xl'">
</tmo-price-lockup>
```