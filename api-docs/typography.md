## Classes

### Size modifiers

You can use these classes on elements or containers to modify the size of their text.

| Font size     | Class         |
| ------------- | ------------- |
| Normal (16px) |               |
| Small (14px)  | `.text-small` |
| Tiny (12px)   | `.text-tiny`  |
| Legal (10px)  | `.text-legal` |

### Headings

Heading styles can be applied by using a heading HTML tag e.g. `<h1>My heading</h1>`, or by using a class e.g. `<h3 class="heading-1">My heading</h3>`. If you use the class, you should still use some type of heading tag.

| Heading type         | Class                   | Element |
| -------------------- | ----------------------- | ------- |
| Heading 1            | `.heading-1`            | h1      |
| Heading 2            | `.heading-2`            | h2      |
| Heading 3            | `.heading-3`            | h3      |
| Heading 4            | `.heading-4`            | h4      |
| Heading 5            | `.heading-5`            | h5      |
| Heading 6            | `.heading-6`            | h6      |
| Section header       | `.section-header`       |         |
| Section header small | `.section-header-small` |         |
| Eyebrow              | `.eyebrow`              |         |

### Display Heading

These should also be applied to a heading tag. Note that these go in the opposite sizing direction to the headings (display 4 is the largest, and display 1 is the smallest), to align with Material Design.

| Heading type | Class        |
| ------------ | ------------ |
| Display 4    | `.display-4` |
| Display 3    | `.display-3` |
| Display 2    | `.display-2` |
| Display 1    | `.display-1` |

### Links

Use `.link` to apply the link style to an anchor element. Anchor elements within a paragraph will automatically have this style applied.

## Utilities

### Alignment

| Alignment | Class          |
| --------- | -------------- |
| Left      | `.text-left`   |
| Right     | `.text-right`  |
| Center    | `.text-center` |

### Font weight

| Font weight | Class                 |
| ----------- | --------------------- |
| 400         | `.font-weight-normal` |
| 700         | `.font-weight-bold`   |
| 800         | `.font-weight-800`    |

### No-wrap

Use `.text-no-wrap` to create a block where the text contents will not break. This is handy for wrapping the T-Mobile name:

```
<span class="text-no-wrap">T-Mobile</span>
```

## Sass Variables

These Sass variables can and should be used in your Sass/scss files to reference conventional typography font families and sizes.

### Font Families

| Sass Variable   | Value                                                                              |
| --------------- | ---------------------------------------------------------------------------------- |
| `$heading-font` | Tele-Grotesk-Next, Arial, Helvetica, 'Helvetica Neue', sans-serif;                 |
| `$body-font`    | BlinkMacSystemFont, -apple-system, Segoe UI, Roboto, Helvetica, Arial, sans-serif; |

### Font sizes

| Sass Variable      | Value |
| ------------------ | ----- |
| `$base-font-size`  | 16px  |
| `$small-font-size` | 14px  |
| `$tiny-font-size`  | 12px  |
| `$legal-font-size` | 10px  |
