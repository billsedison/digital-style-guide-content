Modals are implemented using Angular Material, visit [Angular Material Dialog](https://material.angular.io/components/dialog/overview) for all functional documentation.

## Imports

```
import {MatDialogModule} from '@angular/material/dialog';
```

## Styling

Modal styles must be applied when you open the dialog using the `modal` class.

```
const dialogRef = this.dialog.open(ExampleModalComponent, {
    panelClass: 'modal',
});
```

See the code tab for a more complete example.
