## Toggle:

HTML file: form-elements.component.html

```
<mat-slide-toggle color="primary">Choice</mat-slide-toggle>
```

## Radio:
      
HTML file: form-elements.component.html

```
<mat-radio-group required="true" value="0" aria-label="radio group">
  <mat-radio-button color="primary" value="1">Option1</mat-radio-button>
  <mat-radio-button color="primary" value="0">Option 2</mat-radio-button>
</mat-radio-group>
```

## Check:
      
HTML file: form-elements.component.html

```
<mat-checkbox color="primary" [value]="Selection">
  Selection
</mat-checkbox>
```
