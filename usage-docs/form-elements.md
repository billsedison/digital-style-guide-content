<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

## Dropdowns
### How to use
Dropdowns are directly related to form fields and often used within forms next to standard form fields.

Dropdowns are activated by clicking, not by hovering; this is an intentional design interaction. Why? We design "mobile first" so, we avoid mixing functional elements and interaction by hovering because it decreases User Experience of users of mobile devices. 

### Best practices
* Dropdown fields should be given ample breathing room and white space to ensure ease of legibility.
* The ordering and hierarchy of dropdown fields should be logical and well thought out to provide the fastest, easiest path to completion for a customer.
* Avoid jumbling, crowding or reordering dropdown fields in a way that doesn’t align to familiar patterns.
* The button displays the current state and a down arrow.
* When a user interacts with the field, a popover covers the field and displays the possible selection.
* Pressing a selection automatically dismisses the menu and updates the button to display this new state.
* Clicking or tapping outside of the dropdown popover also dismisses the menu, returning it to its prior state.
* Pressing a dropdown that is filled re-opens the menu. The current active selection must be visually represented as selected or highlighted.
* Scrolling within the dropdown behaves the same way that menus scroll. If the height of a menu prevents all menu items from being displayed, the menu can scroll internally. One example is when viewing a long list of state names.
* Items that are irrelevant to the current context may be removed. 
* Items which are relevant but need certain conditions to be met may be disabled.
* When implemented on mobile devices, dropdowns can invoke the phone’s native selector functionality.
	
### Accessibility

* Should be easily identifiable with a large, tappable touch target.
* Should be easily found on a page among other elements.
* Should clearly indicate their state – whether enabled or disabled, empty or filled, valid or invalid – with clear label, input, and assistive text.

## Text Fields
	
### How to use
Text fields allow users to input information and interact with the site, whether it is a single text field or a longer sign-up form. They are very often used in association with Dropdowns.

### Best practices
* Whenever possible only ask for the minimum required information. Keep the form to the shortest, most efficient design possible.
* For maximum legibility, stack form fields vertically so that a user’s eye can easily scan it from top to bottom
* Use plain, simple language in to describe the required inputs.
* Utilize sentence case.
* Break up long forms whenever possible with contextual titles or sets.
* Use autocomplete wherever possible.
* Use native device functionality wherever possible.
* Pre-format fields wherever possible.
* Build in affordances to speed up field entry. For example do not make a user type dashes while entering their phone number.
* Designs should be clean and clutter-free to provide maximum legibility.
* Form fields should be given ample breathing room and white space to ensure ease of scanning.
* The ordering and hierarchy should be logical and well thought out. Avoid jumbling, crowding or reordering form fields in a way that doesn’t align to common patterns.
* It is preferred for any long forms to be styled on a white or light background to ensure clear legibility and clear action.
* If for a stylistic reason a form is displayed on a black, dark, or image background try to keep the input fields to less than eight. 

</div>