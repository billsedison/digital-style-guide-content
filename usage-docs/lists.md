All HTML would live in this file: list.component.html

### Single line snippet - to meet GLUE specifications, remove Avatar and Icons as needed

```
<mat-nav-list role="list">
  <mat-list-item>
    <img matListAvatar src="" alt="fpo">
    <h3 class="display-6" matLine>Title </h3>
    <span class="icon-wrap">
      <mat-icon matListIcon color="primary" svgIcon=""></mat-icon>
      <mat-icon matListIcon class="list-chevron"
        svgIcon="chevron-right"></mat-icon>
    </span>
    <mat-divider></mat-divider>
  </mat-list-item>
</mat-nav-list>
```
      
### Double line snippet

```
<mat-nav-list role="list">
  <mat-list-item>
    <img matListAvatar src="" alt="fpo">
    <h3 class="display-6" matLine>Title </h3>
    <p class="body" matLine>Subject </p>
    <span class="icon-wrap">
      <mat-icon matListIcon color="primary" svgIcon=""></mat-icon>
      <mat-icon matListIcon class="list-chevron"
        svgIcon="chevron-right"></mat-icon>
    </span>
    <mat-divider></mat-divider>
  </mat-list-item>
</mat-nav-list>
```

### Triple line snippet

```
<mat-nav-list role="list">
  <mat-list-item>
    <img matListAvatar src="" alt="fpo">
    <h3 class="display-6" matLine>Title </h3>
    <p class="body" matLine>Subject </p>
    <p class="body" matLine>Content </p>
    <span class="icon-wrap">
      <mat-icon matListIcon color="primary" svgIcon=""></mat-icon>
      <mat-icon matListIcon class="list-chevron"
        svgIcon="chevron-right"></mat-icon>
    </span>
    <mat-divider></mat-divider>
  </mat-list-item>
</mat-nav-list>
```
