<div style="width:660px; ">

<h1>Magenta</h1>
<p> The preferred logo is Magenta. The logo is only used in the header. Do not use the logo elsewhere.

We do not use the black T-Mobile logo.</p>
<h3>T logo</h3>
<img src="https://cdn.tmobile.com/content/dam/t-mobile/ntm/branding/logos/corporate/tmo-logo-v3.svg" width=75px/>	
	<h3>Full logo</h3><p>We typically don't use the full logo on our digital properties.</p>
<img style="display: block" src="https://www.t-mobile.com/content/dam/t-mobile/corporate/media-library/public/pictures/logos/T-Mobile_New_Logo_Primary_RGB_M-on-W_Transparent.png/_jcr_content/renditions/original" height=50px/>

<h1>Black</h1>
<h3>T logo</h3>
<img src="https://cdn.tmobile.com/content/dam/t-mobile/ntm/branding/logos/corporate/tmo-logo-white-v3.svg" width=75px/>
<h3>Full logo</h3>
	<h4>on transparent</h4>
<img style="display: block" src="https://www.t-mobile.com/content/dam/t-mobile/corporate/media-library/public/pictures/logos/T-Mobile_New_Logo_Secondary_RGB_K-on-W_Transparent.png/_jcr_content/renditions/original" height=50px/> 
	<h4>on white</h4>
<img style="display: block" src="https://www.t-mobile.com/content/dam/t-mobile/corporate/media-library/public/pictures/logos/T-Mobile_New_Logo_Secondary_RGB_K-on-W.jpg/_jcr_content/renditions/original" height=50px/>	
	
</div>