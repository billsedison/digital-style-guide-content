<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">


### What are Checkboxes?

Checkboxes are a UI component that signify whether a given option (represented by an associated label) is on or off.

Checkboxes are most effectively used for opt in options (like on terms & conditions) or lists where the user can select any number of options from a set.

Control should be to the left of the label. The spacing between the label and checkbox is 16px.


In all cases, selection controls should be stacked vertically.


### Best practices

#### Do
* Use checkboxes when the user must select any number of options from a set

#### Don't

</div>