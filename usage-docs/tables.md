<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What are Tables?

Tables organize data into grids for enhanced readability so that users can look for data-informed patterns or trends. They function similar to an accordion in the way users can show or hide content.

Tables are comprised of rows and columns. 

### Best Practices

#### Do

* Clearly label rows and columns
* Use appropriate units of measure

#### Don't

* Use tables to display non-data content
</div>