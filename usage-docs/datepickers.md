<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=8Ij1UVYPir&entryId=38e69bbf-5068-4680-b7c2-81659b332140&cb=1569255180376" width=200px />
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=8Ij1UVYPir&entryId=4b0afbb1-24b4-485f-9183-71ec49c30a9c&cb=1569255180374" width=200px />
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=8Ij1UVYPir&entryId=260ec7de-04a8-42da-b59f-259b1485a480&cb=1569255180376" width=200px />
	
 ### What is a Datepicker?
	
Datepickers allow a user to select a single calendar date. Datepickers appear as drop down below a form field. Users may select the date from the dropdown or use the keyboard to type it in manually.

### Best practices


#### Do
- Use the date picker when the user must select a date.
- If the user must select multiple dates, use multiple Datepickers.


#### Don’t
- Use the Datepicker without a dropdown box. The two must always be paired

	</div>