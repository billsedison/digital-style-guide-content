<style>
.circle{
	border-radius: 100px;
	margin-right: 24px;
}
</style>
<style>
.large {
	width: 200px;
	height: 200px;
}
</style>
<style>
.small {
	width: 125px;
	height: 125px;
	margin-top: 24px;
}
</style>
<style>
.tiny {
	width: 75px;
	height: 75px;
	margin-top: 24px;
	border: 1px solid #CCCCCC;
}
<style>
.shortParagraph{
	width: 550px;
	}
</style>
<style>
.indent{
	margin-left: 56px;
}
</style>
<div style="line-height: 1.6; margin-left:80px; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

<div style="display: flex; margin-top: 80px; flex-direction: row">
	<div class="large circle" style="background-color: #E20074"></div>
	<div class="large circle" style="background-color: #000000"></div>
	<div class="large circle" style="background-color: #FFFFFF; border: 1px solid #CCCCCC;"></div>
</div>
<h1 style="margin-top: 80px">Brand colors</h1>
	
<div style="display: flex; margin-top: 56px">
	<div class="small circle" style="background-color: #E20074"></div>
	<div style="margin-left: 56px">
		<p class="shortParagraph"><strong>Magenta</strong> is our primary brand color. In web, we use it to signify interactivity. </p>
		<div style="display: flex">
			<div>
				<p>HEX</p>
				<p><strong>#E20074</strong></p>
			</div>
			<div class="indent">
				<p>RGB</p>
				<p><strong>226, 0, 116</strong></p>
			</div>
		</div>
 </div>
</div>

<div style="display: flex; margin-top: 56px">
	<div class="small circle" style="background-color: #000000"></div>
	<div style="margin-left: 56px">
		<p class="shortParagraph"><strong>Black</strong> is our secondary brand color. It is the primary background color in dark themes.</p>
		<div style="display: flex">
			<div>
				<p>HEX</p>
				<p><strong>#000000</strong></p>
			</div>
			<div class="indent">
				<p>RGB</p>
				<p><strong>255, 255, 255</strong></p>
			</div>
		</div>
 </div>
</div>	

<div style="display: flex; margin-top: 56px">
	<div class="small circle" style="background-color: #FFFFFF; border: 1px solid #CCCCCC"></div>
	<div style="margin-left: 56px">
		<p class="shortParagraph"><strong>White</strong> is another secondary brand color. It is the primary text color in dark themes.</p>
		<div style="display: flex">
			<div>
				<p>HEX</p>
				<p><strong>#FFFFFF</strong></p>
			</div>
			<div class="indent">
				<p>RGB</p>
				<p><strong>255, 255, 255</strong></p>
			</div>
		</div>
 </div>
</div>		

<h2 style="margin-top: 80px">Variations</h2>

<div style="display: flex; margin-top: 56px">
	<div class="small circle" style="background-color: #F66DB3"></div>
	<div style="margin-left: 56px">
		<p class="shortParagraph"><strong>Magenta Light</strong> is reserved for hover states & animation. </p>
		<div style="display: flex">
			<div>
				<p>HEX</p>
				<p><strong>#F66DB3</strong></p>
			</div>
			<div class="indent">
				<p>RGB</p>
				<p><strong>226, 0, 116</strong></p>
			</div>
		</div>
 </div>
</div>

<div style="display: flex; margin-top: 56px">
	<div class="small circle" style="background-color: #BA0060"></div>
	<div style="margin-left: 56px">
		<p class="shortParagraph"><strong>Magenta Dark</strong> is reserved for hover states & animation. </p>
		<div style="display: flex">
			<div>
				<p>HEX</p>
				<p><strong>#BA0060</strong></p>
			</div>
			<div class="indent">
				<p>RGB</p>
				<p><strong>186, 0, 96</strong></p>
			</div>
		</div>
 </div>
</div>	

<div style="display: flex; margin-top: 56px">
	<div class="small circle" style="background-color: #A10053;"></div>
	<div style="margin-left: 56px">
		<p class="shortParagraph"><strong>Magenta Very Dark</strong> is reserved for hover states & animation. </p>
		<div style="display: flex">
			<div>
				<p>HEX</p>
				<p><strong>#A10053</strong></p>
			</div>
			<div class="indent">
				<p>RGB</p>
				<p><strong>161, 0, 83</strong></p>
			</div>
		</div>
 </div>
</div>	
<h2 style="margin-top: 80px">Grays</h2>

<div style="display: flex; margin-top: 40px; flex-direction: row; width: 660px; flex-wrap: wrap;">
	<div class="tiny circle" style="background-color: #F8F8F8"></div>
	<div class="tiny circle" style="background-color: #F2F2F2"></div>
	<div class="tiny circle" style="background-color: #E8E8E8"></div>
	<div class="tiny circle" style="background-color: #CCCCCC"></div>
	<div class="tiny circle" style="background-color: #9B9B9B"></div>
	<div class="tiny circle" style="background-color: #6A6A6A"></div>
	<div class="tiny circle" style="background-color: #4C4C4C"></div>
	<div class="tiny circle" style="background-color: #333333"></div>
	<div class="tiny circle" style="background-color: #262626"></div>
</div>

<h2 style="margin-top: 80px">Accent colors</h2>

<div style="display: flex; margin-top: 40px; flex-direction: row; width: 660px; flex-wrap: wrap;">
	<div class="tiny circle" style="background-color: #078A14"></div>
	<div class="tiny circle" style="background-color: #51D85E"></div>
	<div class="tiny circle" style="background-color: #E3B721"></div>
	<div class="tiny circle" style="background-color: #E8200D"></div>
</div>
	
</div>