<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

#### Usage
Pagination UI appears below the content of a page and indicates to the user that there are more pages of content beyond this one. This is most commonly used in search results. For more information on search, see the <a href="../components/search/usage">Search</a> article.
* Clicking on a page number should change to the appropriate content.
* The active page is represented by a box with a magenta stroke and page number.
* On hover or click, the surrounding box state becomes a fill.
* Prioritize the list of items on the page.

### Best practices
#### Do
* Use to break up lists, such as ecommerce category pages, search engine results pages, article archives, and image galleries.

#### Don't
* Don’t use to break up articles. Use a single page for all article content.

</div>