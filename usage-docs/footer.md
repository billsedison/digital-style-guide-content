<div class="banner banner-warning"><!----><div class="banner-icon" style=""><mat-icon class="mat-icon material-icons mat-icon-no-color" role="img" aria-hidden="true">error_outline</mat-icon></div><div class="banner-content"> This <strong>Footer</strong> section is still under construction and consideration. Please don't use it for design or development guidance</div></div>

### Overview

The universal footer appears at the bottom of the page and provides navigation links to the different parts of the site. 

* Changes to the footer should not be made without consulting the appropriate product owner.
* The footer links and content may not change.

Behavior on mobile:
The links collapse into groups. 

### Best practices

#### Do
* Place the footer 1XL spacer from the bottom of the content.

#### Don't
* Place any other content below the footer.
	

</div>