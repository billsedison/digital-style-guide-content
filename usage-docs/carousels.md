<div class="banner banner-warning"><div class="banner-icon"><mat-icon class="mat-icon material-icons mat-icon-no-color" role="img" aria-hidden="true">error_outline</mat-icon></div><div class="banner-content"> This <strong>Carousels</strong> section is still under construction and consideration. Please don't use it for design or development guidance</div></div>

### What are carousels?
	
Carousels are UI elements containing several pages of browsable content. Some carousels have timers that automatically advance content.  They contain large imagery and text to draw the user's attention and to provide an overview of key T-Mobile offerings and features.

Carousels should be used very sparingly. See: <a href = "http://shouldiuseacarousel.com/" target="_blank" >http://shouldiuseacarousel.com/</a>



### Best practices


#### Do

#### Don’t
* Use carousels
	
</div>