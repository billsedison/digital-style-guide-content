<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### How to use
Text fields allow users to input information and interact with the site, whether it is a single text field or a longer sign-up form. They are very often used in association with Dropdowns.

### Best practices
#### Do
* Break up long forms whenever possible with contextual titles or sets.
* On mobile, use fewer forms if possible. For example, use "Name" rather than "First name" and "Last name".

	
Text fields allow the user to input text via the keyboard. The user must first click or tap on the text field and then begin to type. The user's input appears in the text field as they type.

### Anatomy

* Text fields are 80px high.
* Desktop recommended width: 2, 3, 6 columns
* Mobile recommended width: 12 columns
* On mobile, form fields should be 12 columns wide.


### Errors

* Errors from typing invalid text input are considered low priority.
* Errors show a red alert icon underneath the text field.
* For accessibility, error messages present on text fields must contain "Error: " before the error message text.

### Variations

* Text fields can include an icon on the right side. This is often used for payments types.
* Text fields may include helper text or a hyperlink underneath. For example helper text might say "Please type in a valid email address." or "No special characters allowed."
* Disabled form fields use a dotted line instead of a solid line.

For additional information on best practices for form design, check out some additional resources here:

https://www.ventureharbour.com/form-design-best-practices/

https://www.formassembly.com/blog/web-form-design/

https://www.nngroup.com/articles/web-form-design/

https://medium.theuxblog.com/10-best-practices-for-designing-user-friendly-forms-fa0ba7c3e01f
	
</div>