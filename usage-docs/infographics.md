<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">


## What are infographics?
	
Infographics display data to the user through graphical visualizations. Infographics can be interactive. They should be used to make trends and patterns in data easily observable. Infographics should be custom designed for the context they are in, and because of this we do not have templates for them.


## Best practices
	
Dos
	
* Label graph axes
* Make graphics visually appealing but also easily understandable

Donts
	
* Use misleading scales (ex: logarithmic instead of linear)
* Let users interact and play with the data
	
</div>