<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What are Badges?	
	
Badges are a component that overlays an icon to indicate that some new activity has occurred. We use Badges in the shop to indicate that the user has placed something in their cart.

### Best practices	
	
#### Do
* Overlay badges over an icon. You may also include a number.

#### Don’t
* Use badges in any other context	
</div>