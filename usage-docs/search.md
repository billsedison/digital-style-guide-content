<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### Overview

The search bar functions like a text field. While the user is typing, autocomplete suggestions must appear underneath the text bar like in the figure below. If the user submits their search, or taps on any of the suggested results, a list of results must appear.

### Best practices

All web-based T-Mobile search features must look and behave like the template shown below. The page contains several organisms.
Results list items must represent pages. Result categories can be separated by tabs like in the example below.

<img src="https://tmobile.egnyte.com/dd/UVcOYkcwNY/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true"/>

</div>