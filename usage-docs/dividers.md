<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What are dividers?
	
Dividers are thin lines used to visually distinguish content. There are 1px and 3px variants. Use dividers sparingly. There are no strict guidelines regarding their usage. 

An effective example of dividers is for use in <a href='../components/search/usage'>Search</a> results lists.

	
</div>
