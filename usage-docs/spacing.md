<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=lfPzycYyMD&entryId=3d7b31a1-886a-45c4-8921-4f177dc5bce7&cb=1570479601268" width=320px/>
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=lfPzycYyMD&entryId=e6e0f8e7-f116-4be9-9278-785f16822109&cb=1570479601270" width=320px/>

## Spacing between elements 

* Header and title: 32px/40px (M)
* Title and body copy: 16px (XS)
* Between Primary & Secondary CTAs: 16px (XS)
* Body copy and next element: 40px (L)
* General: 24px
* CTAs and next interactive element: 24px (at least)
* Tertiary buttons: 24px (including label)
	
### In lists
* Section headers and list content underneath: 8px
* Section headers and copy underneath: 8px
* Space between sections: 24px
* Space between list item and legal text: 16px
	
	
<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=lfPzycYyMD&entryId=1ad88133-867e-4a1e-99b0-321b714f68f7&cb=1570479601270" width=300px/>

</div>