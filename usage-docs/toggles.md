<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What are Toggles?

Use for settings with an ON/OFF relationship. Control should be to the right of the label.

Toggles are a UI component that signify whether a given option (represented by an associated label) is on or off.

Toggles are most effectively used for settings with a higher level of permanence.

In all cases, selection controls should be stacked vertically.
	
</div>