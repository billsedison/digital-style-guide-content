<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

## Responsive strategy
T-Mobile uses a 12 column responsive grid. Responsive grids eliminate the need to define component widths by pixels. As modern screens can now be any number of pixels in width, responsive grids help reduce the amount of design work required to build a design for multiple form factors.

T-Mobile uses a mobile-first approach to web design. This means experiences should be designed for mobile as the target platform, and then adapted to desktop (and other sizes) later.

### Breakpoints
Our screen width breakpoints are 320 (Mobile Small), 375 (Mobile Large), 768 (Tablet), 1024 (Desktop Small), 1280 (Desktop), and 1680 (Desktop Large) pixels.

For the sake of simplicity in design files, we design at 375 for mobile web and 1280 for desktop.
	
Certain elements use a [fluid grid](https://getbootstrap.com/docs/4.3/layout/overview/). This is a grid without gutters on the left and right sides.

In this style guide, some elements have widths defined by fluid columns.

For specific column and gutter width by screen size, please reference the Sketch library.

### Best Practices
All responsive design must take the dynamic scaling of content into consideration. Pay special attention to how content will expand, collapse, and wrap on mobile and desktop devices.

As a general rule, our content is left-aligned. Use center-aligned content sparingly.

For Desktop, content should exist within the leftmost 6 columns. In the below example, copy blocks are 6 responsive columns wide.

<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=NLxgMRTjmA&entryId=40a823b8-a92a-4b21-892a-83b09096ce7d&cb=1570478388752" width=660px/>

For Mobile, content should exist within all 12 responsive columns.

<img src="https://tmobile.egnyte.com/openpublicdocument.do?thumbNail=true&w=1200&h=1200&type=proportional&forceDownload=false&link_id=NLxgMRTjmA&entryId=db739ae0-1040-483b-82e1-430447bd595f&cb=1570478388754" width=330px/>


	
</div>