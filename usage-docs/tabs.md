<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What is a Tab?
Tabs help organize page content. They maintain a master/detail relationship with content immediately underneath. The tabs above illustrate this relationship.
	
Tab labels should be 24px apart. 

The selected tab should appear in magenta on light themes and white on dark themes. The selected tab has an underline.

### Mobile

On mobile, tabs should be inset to the first responsive column so they don't fall into gutters on the right and left of the screen.
	
<img src="https://tmobile.egnyte.com/dd/FD7GkzYnuK/?thumbNail=1&w=1200&h=1200&type=proportional&preview=true" width=300px/>
	
	
### Best Practices
#### Do
* Each tab should have a distinct category.
* Tab categories should be of relatively equal importance. There is no implicit hierarchy within tabs.

#### Don't
* Tabs should not be represent steps in a procedure.
* In our styling, tabs should not contain icons.
</div>