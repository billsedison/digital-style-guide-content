<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

## What are links?
	
Links are used to navigate the user to a different location from within a paragraph of text. If your designs require linking the user to another page in some other context, we recommend looking at the <a href="components/buttons/usage"> Buttons</a> page.
	
### Styles of links

The size and font of the link should match the size and font of the containing paragraph. The color must be either magenta or black (white in dark themes). Magenta should be used for high-importance links while black should be used for low-importance links. Classification of the link's importance is left to the designer's discretion.
	
### Best practices
#### Do
* 	Use links exclusively in-line with other copy.
	
#### Don't
* Use links by themselves. Consider using a Button instead.
</div>