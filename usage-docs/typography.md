<style>
	.tele{
		font-family: telegrotesknext-ultra, Papyus;
		margin-bottom: 10%;
	}
</style>
<style>
	.system{
		font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif
	}
</style>
<style>
	.big{
	 font-size: 200px;
	 line-height: 80%;
	margin: 120px 0px 80px 0px;
	}
</style>
<style>
	.textStyle{
	 font-size: 16px;
	 color: #262626;
	 width: 500px;
	box-sizing: border-box;
	}
</style>
<div class="system" style="width: 1000px; margin-left: 80px">

<p class="big tele">AaBbCcDd</p>
	<p style="width: 600px;"><strong>TeleGrotesk Next Ultra</strong> is our typeface for headers. This typeface is proprietary to T-Mobile and a big part of our brand.</p>

<div style="display: flex;  flex-wrap: wrap">
	<div class="textStyle">
	<p class="tele" style="font-size: 72px; margin-top: 56px;">Display 1</p>
	<p><strong>Font family:</strong> TeleGrotesk Next Ultra</br>
	<strong>Size:</strong> 72px</br>
	<strong>Color:</strong> #262626, #E20074</strong></br>
	<strong>Letter spacing:</strong> -1.45px</strong></br>
  <strong>Line height:</strong> 72px</br></p>
		<p>Use Display 1 for marketing purposes only.</p>
</div>
<div class="textStyle">
 <p class="tele" style="font-size: 56px; margin-top: 56px;">Display 2</p>
	<p><strong>Font family:</strong> TeleGrotesk Next Ultra</br><strong>Size:</strong> 56px</br>
	<strong>Color:</strong> #262626, #E20074</strong></br>
	<strong>Letter spacing:</strong> -1.1px</strong></br>
  <strong>Line height:</strong> 56px</br></p>
 <p>Use this for the first page in a flow.</p>
</div>
<div class="textStyle">
<p class="tele" style="font-size: 48px; margin-top: 56px;">Display 3</p>
	<p><strong>Font family:</strong> TeleGrotesk Next Ultra</br><strong>Size:</strong> 48px</br>
	<strong>Color:</strong> #262626, #E20074</strong></br>
	<strong>Letter spacing:</strong> -0.4px</strong></br>
  <strong>Line height:</strong> 52px</br></p>
 <p>Use this for the first page in a flow.</p>
</div>
<div class="textStyle">
	<p class="tele" style="font-size: 40px; margin-top: 56px;">Display 4</p>
			<p><strong>Font family:</strong> TeleGrotesk Next Ultra</br><strong>Size:</strong> 40px</br>
	<strong>Color:</strong> #262626, #E20074</strong></br>
	<strong>Letter spacing:</strong> -0.22px</strong></br>
  <strong>Line height:</strong> 44px</br></p>
<p>This is the preferred title size.</p>
</div>
<div class="textStyle">
 <p class="tele" style="font-size: 32px; margin-top: 56px;">Display 5</p>
	<p><strong>Font family:</strong> TeleGrotesk Next Ultra</br><strong>Size:</strong> 32px</br>
	<strong>Color:</strong> #262626, #E20074</strong></br>
	<strong>Letter spacing:</strong> 0px</strong></br>
  <strong>Line height:</strong> 36px</br></p>
 <p>Use this in the same context as Display 4 for long titles.</p>
</div>
<div class="textStyle">
 <p class="tele" style="font-size: 24px; margin-top: 56px;">Display 6</p>
	<p><strong>Font family:</strong> TeleGrotesk Next Ultra</br><strong>Font:</strong> 24px</br>
	<strong>Color:</strong> #262626, #E20074</strong></br>
	<strong>Letter spacing:</strong> 0px</strong></br>
  <strong>Line height:</strong> 28px</br></p>
 <p></p>
</div>
</div>


<h1 style="margin-top: 120px">System fonts</h1>	
<p> For everything else, we use system fonts. What does that mean? Well, your typeface should depend on platform.</p>
<ul>
	<li>On iOS, use <strong>SF Pro</strong>.</li>
	<li>On Android, use <strong>Roboto</strong>.</li>
	<li>On MacOS, use <strong>Helvetica Neue</strong>.</li>
	<li>On Windows, use <strong>Segoe UI</strong>.</li>
</ul>


<div style="display: flex;  flex-wrap: wrap">
	<div class="textStyle">
	 <p class="system" style="font-size: 32px; margin-top: 32px;">Section header big</p>
	 	<p><strong>Font family:</strong> Default system font</br>
			<strong>Size:</strong> 32px</br>
			<strong>Color:</strong> #262626</strong></br>
			<strong>Letter spacing:</strong> auto</strong></br>
			<strong>Line height:</strong> auto</p>
		<p></p>
  </div>
	<div class="textStyle">
		<p class="system" style="font-size: 24px; margin-top: 32px;">Section header small</p>
		<p><strong>Font family:</strong> Default system font</br>
			<strong>Size:</strong> 24px</br>
			<strong>Color:</strong> #262626</strong></br>
			<strong>Letter spacing:</strong> auto</strong></br>
			<strong>Line height:</strong> 32px</br></p>
	</div>
	<div class="textStyle">
		<p class="system" style="font-size: 16px; margin-top: 32px;">Body</p>
		<p><strong>Font family:</strong> Default system font</br>
			<strong>Size:</strong> 16px</br>
			<strong>Color:</strong> #262626, #6A6A6A</strong></br>
			<strong>Letter spacing:</strong>auto</strong></br>
			<strong>Line height:</strong> 24px</br></p>
	</div>
	<div class="textStyle">
		<p class="system" style="font-size: 14px; margin-top: 32px;">Body small</p>
		<p><strong>Font family:</strong> Default system font</br>
			<strong>Size:</strong> 14px</br>
			<strong>Color:</strong> #262626, #6A6A6A</strong></br>
			<strong>Letter spacing:</strong> auto</strong></br>
			<strong>Line height:</strong> 22px</br></p>
	</div>
	<div class="textStyle">
		<p class="system" style="font-size: 12px; margin-top: 32px;">Body tiny</p>
		<p><strong>Font family:</strong> Default system font</br>
			<strong>Size:</strong> 12px</br>
			<strong>Color:</strong> #262626, #6A6A6A</strong></br>
			<strong>Letter spacing:</strong> auto</strong></br>
			<strong>Line height:</strong> 18px</br></p>
	</div>
	<div class="textStyle">
		<p class="system" style="font-size: 10px; margin-top: 32px;">Legal</p>
		<p><strong>Font family:</strong> Default system font</br>
			<strong>Size:</strong> 10px</br>
			<strong>Color:</strong> #262626</strong></br>
			<strong>Letter spacing:</strong> auto</strong></br>
			<strong>Line height:</strong> 12px</br></p>
	</div>
</div>
</div>