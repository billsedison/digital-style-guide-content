<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### Usage

Breadcrumbs are used to track the user's position within the page hierarchy.  This is mostly valuable in sub-pages multiple layers deeper than the home page.
 
* Must always appear below the header and above the page title.
* Not a page requirement. 
* Encouraged if their use augments the page's usability.
* Clicking or tapping on a breadcrumb must navigate the user to the page the breadcrumb represents.
 
#### Behavior
Breadcrumbs are interactive. If a page title is too long, or the user is using a mobile device, previous pages can be bundled into a dropdown menu.


### Best Practices
#### Do

* Use on pages several layers deep within the site map.
* Use the page titles for breadcrumbs

#### Don't

* Use Breadcrumbs to track the user's navigation path. They track the user's location within the site map, regardless of which page the user came from previously.
* Use excessively long page titles for breadcrumbs.

	
	
</div>