<div style="width:660px; line-height: 1.6; font-family: BlinkMacSystemFont,-apple-system,Segoe UI,Roboto,Helvetica,Arial,sans-serif">

### What is a progress bar?


Progress bars represent where the user is within a multi-step flow, like a wizard. Progress bars may have labels, but they are not required. If a progress bar has labels on desktop, they may be hidden on mobile.
	
#### Behavior
On mobile, the user should be able to use a swipe gesture to move to the previous step in the wizard. The user cannot swipe forward to a step they haven't been on. The user cannot swipe all the way out of the wizard, either. The back gesture should do nothing on the first page of a wizard.
	

#### Do
* Progress bars should only be used when there are 2+ steps in a flow.
* Progress bars should appear beneath the header.
* Progress bars are full width on both desktop and mobile.

#### Don’t
* Progress bars are not loading bars. Do not use them to indicate system loading, or use them with animation.
* Progress bars are not interactive. Pressing on the different steps should do nothing. To move backwards in the flow, the user should use the browser back button, or the hardware back button on Android devices.

### Anatomy

Desktop width: 12 fluid columns (full width)
Mobile width: 12 fluid columns (full width)
</div>